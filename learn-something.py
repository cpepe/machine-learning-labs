import math
import numpy
import pandas
import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers import Dense
import sys

FILENAME = 'data/something.csv'


# fix random seed for reproducibility
numpy.random.seed(7)
# load the dataset
dataframe = pandas.read_csv(FILENAME, engine='python')
dataset = dataframe.values
dataset = dataset.astype('float32')

train_size = int(len(dataset) * 0.67)
test_size = len(dataset) - train_size
train, test = dataset[0:train_size,:], dataset[train_size:len(dataset),:]

# reshape data
trainX = train[:,0:2]
trainY = train[:,2]
testX = test[:,0:2]
testY = test[:,2]

# create and fit Multilayer Perceptron model
model = Sequential()
model.add(Dense(8, input_dim=2, activation='relu'))
model.add(Dense(10, init='uniform', activation='relu'))
model.add(Dense(10, init='uniform', activation='relu'))
model.add(Dense(10, init='uniform', activation='relu'))
model.add(Dense(1))
model.compile(loss='mean_squared_error', optimizer='adam')

def train_loop():
	model.fit(trainX, trainY, nb_epoch=200, batch_size=2, verbose=2)
	# Estimate model performance
	trainScore = model.evaluate(trainX, trainY, verbose=0)
	print('Train Score: %.2f MSE (%.2f RMSE)' % (trainScore, math.sqrt(trainScore)))
	testScore = model.evaluate(testX, testY, verbose=0)
	print('Test Score: %.2f MSE (%.2f RMSE)' % (testScore, math.sqrt(testScore)))
	# generate predictions for training
	trainPredict = model.predict(trainX)
	testPredict = model.predict(testX)
	# shift train predictions for plotting
	# trainPredictPlot = numpy.empty_like(dataset)
	# trainPredictPlot[:, :] = numpy.nan
	# trainPredictPlot[look_back:len(trainPredict)+look_back, :] = trainPredict
	# # shift test predictions for plotting
	# testPredictPlot = numpy.empty_like(dataset)
	# testPredictPlot[:, :] = numpy.nan
	# testPredictPlot[len(trainPredict)+(look_back*2)+1:len(dataset)-1, :] = testPredict
	# plot baseline and predictions
	plt.clf()
	plt.cla()
	#plt.plot(dataset)
	plt.plot(trainY)
	plt.plot(trainPredict)
	plt.plot(testPredict)
	plt.show()
	plt.clf()
	plt.cla()
	error = list()
	for i in range(0, len(test)-1):
		error.append(test[i] - testPredict[i])
	plt.plot(error)
	plt.show()

for i in range(0,10):
	train_loop()

