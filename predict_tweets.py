import numpy as np
import gensim
from keras.models import model_from_json

tfidf_file = './models/twitter-sentiment/tfidf.npy'
w2v_model_file = './models/twitter-sentiment/word2vec.model'
keras_model_json_file = './models/twitter-sentiment/nn-model.json'
keras_model_h5_file = './models/twitter-sentiment/nn-model.h5'

tweet_w2v = None
tfidf = None

def buildWordVector(tokens, size):
    vec = np.zeros(size).reshape((1, size))
    count = 0.
    for word in tokens:
        try:
            vec += tweet_w2v[word].reshape((1, size)) * tfidf[word]
            count += 1.
        except KeyError: # handling the case where the token is not
                         # in the corpus. useful for testing.
            continue
    if count != 0:
        vec /= count
    return vec

def get_model():
    #load word2vec model
    global tweet_w2v
    tweet_w2v = gensim.models.Word2Vec.load(w2v_model_file)
    #load tfidf dictionary
    global tfidf
    tfidf = np.load(tfidf_file).item()
    # load json and create model
    json_file = open(keras_model_json_file, 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    model = model_from_json(loaded_model_json)
    # load weights into new model
    model.load_weights(keras_model_h5_file)
    print("Loaded model from disk")
    model.compile(optimizer='rmsprop',
                loss='binary_crossentropy',
                metrics=['accuracy'])
    return model

if __name__ == "__main__":
    model = get_model()
    print(tweet_w2v.most_similar('nice'))
    print(tweet_w2v.most_similar('love'))
    print(tweet_w2v.most_similar('kittens'))

    #let's run some model predictions here
    sentiments = {0: 'negative', 1: 'positive'}
    tests = [
        'this is the worst day of my life',
        'FUCK TRUMP',
        'I love kittens so much',
        'Let us be clear, this was not an economic decision. It is nothing more than a cruel ploy to inspire fear and anger towards immigrants.',
        'Lots of people are making moves lately. So am I. https://dave.cheney.net/2017/09/06/why-i-joined-heptio',
        'Nice',
        'stopped to fish for 10min and dropped my key in the raging river. Got a little wet but found the keys. No fish. Calling the day a win.',
        'I have been keeping up with the news online since I have been sick, but to hear it all in 5 min on an @NPR update was overwhelming. Help us all.',
        'It matters less who owns CX and more who executes on it - @Tiffani_Bova via @deniseleeyohn #CX https://buff.ly/2j3jJS9'
    ]
    print tests
    vTest = [buildWordVector(tw, 200) for tw in tests]
    for i in range(len(vTest)):
        p = model.predict(vTest[i])
        print('[%0.2f] %s: %s' % (p[0][0], sentiments[ np.round(p[0][0]) ], tests[i] ))
