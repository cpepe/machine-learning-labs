#kera, theano example: http://machinelearningmastery.com/tutorial-first-neural-network-python-keras/
# training data is here: http://archive.ics.uci.edu/ml/machine-learning-databases/pima-indians-diabetes/pima-indians-diabetes.data
# predict_me.csv is a subset of the training data
#
## Dev setup:
#virtualenv nn
#source nn/bin/activate
#pip install keras
# "backend": "theano" in ~/.keras/keras.json

from keras.models import Sequential
from keras.layers import Dense
import numpy as np
# fix random seed for reproducibility
seed = 7
np.random.seed(seed)

# load pima indians dataset
dataset = np.loadtxt("data/pima-diabetes.csv", delimiter=",")
# split into input (X) and output (Y) variables
X = dataset[:,0:8]
Y = dataset[:,8]

# create model
model = Sequential()
model.add(Dense(12, input_dim=8, init='uniform', activation='relu'))
model.add(Dense(8, init='uniform', activation='relu'))
model.add(Dense(1, init='uniform', activation='sigmoid'))

# Compile model
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

# Fit the model
model.fit(X, Y, nb_epoch=150, batch_size=10)

# evaluate the model
scores = model.evaluate(X, Y)
print("%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))

#follow on
# # calculate predictions
futureset = np.loadtxt("data/predict_me.csv", delimiter=",")
fX = futureset[:,0:8]
fActual = futureset[:,8]
predictions = model.predict(fX)
# # round predictions
rounded = [round(x) for x in predictions]
guesses = zip(fActual, rounded)
print np.vstack(guesses)
print "Accurately guessed %s%% of the time" % ( int( 100*np.equal(fActual,rounded).sum()/len(rounded) ))



