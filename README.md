# Labs #
In college I studied neural networks for non-linear control systems (robots) and it was awesome. Life happened and I didn't stick with it. This is rebooting my efforts since there ain't no learnin like deep learnin.

### What is this repository for? ###
* One off scripts, tutorials, experiments
* May drift off into other data science disciplines but should stay somewhat focused around bettering myself ;-)

### How do I get set up? ###
I don't know why you would want to but initially you'll need matplotlib, numpy, and the other packages you'll see in every neural network project out there. I think I have scipy and anaconda but I'm not sure exactly what the other deps are. Run it and keep installing things until it works I guess. Start with virtualenv so you don't end up with a mess like I have.

### Who do I talk to? ###
* Twit@cpepe