
# convert an array of values into a dataset matrix
def create_dataset(dataset, look_back=1):
	dataX, dataY = [], []
	for i in range(len(dataset)-look_back-1):
		a = dataset[i:(i+look_back), 0]
		dataX.append(a)
		dataY.append(dataset[i + look_back, 0])
	return numpy.array(dataX), numpy.array(dataY)

#in case I need to use this again
def sine_wave(repeat=1):
	import math
	pi=math.pi
	for i in range(0,repeat*360):
		print math.sin(i*pi/180)

def somepattern():
	from math import pow
	for i in range(0,10):
		for j in range(0,10):
			print '%s,%s,%s' % (i,j, 2*i + pow(j, 1/(1+(j+i)%4) ))



if __name__ == "__main__":
	#sine_wave(10)
	somepattern()