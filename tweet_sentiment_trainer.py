'''
some modifications made due to omission and api updates to packages.

original post: http://ahmedbesbes.com/sentiment-analysis-on-twitter-using-word2vec-and-keras.html
data: https://disq.us/url?url=https%3A%2F%2Fgithub.com%2Fadamwulf%2Fmovie-review-sentiment-data%3AYBq0BkAdLAo2nsan5K1NfTiJnq8&cuid=4063800
'''

import pandas as pd # provide sql-like data manipulation tools. very handy.
pd.options.mode.chained_assignment = None
import numpy as np # high dimensional vector computing library.
from copy import deepcopy
from string import punctuation
from random import shuffle
from sklearn.preprocessing import scale
import gensim
from gensim.models.word2vec import Word2Vec # the word2vec model gensim class
from keras.models import Sequential, model_from_json
from keras.layers import Dense

LabeledSentence = gensim.models.doc2vec.LabeledSentence # we'll talk about this down below

from tqdm import tqdm
tqdm.pandas(desc="progress-bar")

from nltk.tokenize import TweetTokenizer # a tweet tokenizer from nltk.
tokenizer = TweetTokenizer(strip_handles=True, reduce_len=True)

from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer

def ingest():
    data = pd.read_csv('./data/tweets/training.1600000.processed.noemoticon.csv')
    data.drop(['ItemID', 'SentimentSource'], axis=1, inplace=True)
    data = data[data.Sentiment.isnull() == False]
    data['Sentiment'] = data['Sentiment'].map(int)
    data['Sentiment'] = data['Sentiment'].map( {4:1, 0:0} ) #convert sentiment of 4 to 1 since we're using sigmoid on output
    data = data[data['SentimentText'].isnull() == False]
    data.reset_index(inplace=True)
    data.drop('index', axis=1, inplace=True)
    print 'dataset loaded with shape', data.shape
    return data

def postprocess(data, n=1000000):
    #a failure of this approach is the first 800k are negative, next 800k are positive
    #so the negative view is unfairly weighted. Better would be a random sample
    #data = data.head(n)
    #instead let's get a random sample from the tweets
    data=data.sample(frac=(n/1600000.0),random_state=200)
    data['tokens'] = data['SentimentText'].progress_map(tokenize)  ## progress_map is a variant of the map function plus a progress bar. Handy to monitor DataFrame creations.
    data = data[data.tokens != 'NC']
    data.reset_index(inplace=True)
    data.drop('index', inplace=True, axis=1)
    return data

def tokenize(tweet):
    try:
        tweet = unicode(tweet.decode('utf-8').lower())
        tokens = tokenizer.tokenize(tweet)
        tokens = filter(lambda t: not t.startswith('@'), tokens)
        tokens = filter(lambda t: not t.startswith('#'), tokens)
        tokens = filter(lambda t: not t.startswith('http'), tokens)
        return tokens
    except:
        return 'NC'

def labelizeTweets(tweets, label_type):
    labelized = []
    for i,v in tqdm(enumerate(tweets)):
        label = '%s_%s'%(label_type,i)
        labelized.append(LabeledSentence(v, [label]))
    return labelized

def getTweetVector(msg):
    """
    return the prediction from the model
    """
    v=buildWordVector(msg.split(), 200)


#FIXME: n not defined (also do a better job making train and test here)
n=1000000
n_dim = 200
data = ingest()
print(data.head(20))
print('...')
print(data.tail(20))
data = postprocess(data, n=n)
x_train, x_test, y_train, y_test = train_test_split(np.array(data.head(n).tokens),
                                                    np.array(data.head(n).Sentiment), test_size=0.2)
x_train = labelizeTweets(x_train, 'TRAIN')
x_test = labelizeTweets(x_test, 'TEST')
print(x_train[0])

tweet_w2v = Word2Vec(size=n_dim, min_count=10)
tweet_w2v.build_vocab([x.words for x in tqdm(x_train)])
print('suggested iterations: %s' % (tweet_w2v.iter))
#xwords = [x.words for x in tqdm(x_train)]
#tweet_w2v.train(xwords, total_words=len(xwords), epochs=8)
tweet_w2v.train([x.words for x in tqdm(x_train)],total_examples=tweet_w2v.corpus_count, epochs=tweet_w2v.iter)

print('Similar to Good:')
print(tweet_w2v.most_similar('good'))
print('Similar to Bad:')
print(tweet_w2v.most_similar('bad'))

tweet_w2v.save('./models/twitter-sentiment/word2vec.model')
print('Saving Word2Vec model')

print 'building tf-idf matrix ...'
vectorizer = TfidfVectorizer(analyzer=lambda x: x, min_df=10)
matrix = vectorizer.fit_transform([x.words for x in x_train])
tfidf = dict(zip(vectorizer.get_feature_names(), vectorizer.idf_))
print 'vocab size :', len(tfidf)
np.save('./models/twitter-sentiment/tfidf.npy', tfidf)
print('Saving tfidf lookup table')

def buildWordVector(tokens, size):
    vec = np.zeros(size).reshape((1, size))
    count = 0.
    for word in tokens:
        try:
            vec += tweet_w2v[word].reshape((1, size)) * tfidf[word]
            count += 1.
        except KeyError: # handling the case where the token is not
                         # in the corpus. useful for testing.
            continue
    if count != 0:
        vec /= count
    return vec

train_vecs_w2v = np.concatenate([buildWordVector(z, n_dim) for z in tqdm(map(lambda x: x.words, x_train))])
train_vecs_w2v = scale(train_vecs_w2v)

test_vecs_w2v = np.concatenate([buildWordVector(z, n_dim) for z in tqdm(map(lambda x: x.words, x_test))])
test_vecs_w2v = scale(test_vecs_w2v)

#load a saved model or not
if False:
    # load json and create model
    json_file = open('./models/twitter-sentiment/model.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    model = model_from_json(loaded_model_json)
    # load weights into new model
    model.load_weights("./models/twitter-sentiment/model.h5")
    print("Loaded model from disk")
else:
    model = Sequential()
    model.add(Dense(32, activation='relu', input_dim=200))
    model.add(Dense(1, activation='sigmoid'))

model.compile(optimizer='rmsprop',
            loss='binary_crossentropy',
            metrics=['accuracy'])

model.fit(train_vecs_w2v, y_train, epochs=9, batch_size=32, verbose=2)

#save the model or not
if False:
    # serialize model to JSON
    model_json = model.to_json()
    with open("./models/twitter-sentiment/model.json", "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights("./models/twitter-sentiment/model.h5")
    print("Saved model to disk")

score = model.evaluate(test_vecs_w2v, y_test, batch_size=128, verbose=2)
print score[1]
print score

print('Negative sentiment')
t = 'fuck trump'
print( model.predict( buildWordVector(t,200) )[0][0] )
print('Positive sentiment')
t = 'Lots of people are making moves lately. So am I. https://dave.cheney.net/2017/09/06/why-i-joined-heptio'
print( model.predict( buildWordVector(t,200) )[0][0] )
print("test with: model.predict( buildWordVector(t,200) )")
import pdb; pdb.set_trace()
