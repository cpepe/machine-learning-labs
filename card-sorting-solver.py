import numpy as np
import pandas

colors = ('red', 'green', 'blue', 'yellow')
shapes = ('square', 'triangle', 'circle', 'cross')
rules = ('color', 'shape', 'count')

"""
here we model the game as a DataFrame instead. If the guess is right then winner == guess for all four cards. Here we are choosing if the Card is a match to each key card (answer T or F for each)

The NN will need 3 input neurons, N hidden, and 4 output neurons. It is unclear how to best account for changing rules without having to fully retrain

import pandas
import numpy as np
df=pandas.DataFrame()
df['card1'] = np.array( (1,2,3) )
df['card2'] = np.array( (2,3,1) )
df['card3'] = np.array( (3,1,2) )
df['card4'] = np.array( (1,2,3) )

guess = np.array( (False, False, True, False) )
winner = df.loc[2] == 2 #this is df.loc[metric] == value
err = (np.array(winner) == guess)
err
array([ True,  True,  True,  True], dtype=bool)
"""

#wisconsin card sorting test.
# pass in values for key-cards, else get a random card
class Card(object):
    def __init__(
                self,
                color=np.random.randint(0,3),
                shape=np.random.randint(0,3),
                count=np.random.randint(1,4)
                ):
        self.color = color
        self.shape = shape
        self.count = count

    def __repr__(self):
        return '%s:%s:%s' % (colors[self.color], shapes[self.shape], self.count)


class GameSet(object):
    def __init__(self):
        self.cards = (
            Card(colors.index('red'), shapes.index('circle'), 1),
            Card(colors.index('green'), shapes.index('triangle'), 2),
            Card(colors.index('blue'), shapes.index('square'), 3),
            Card(colors.index('yellow'), shapes.index('cross'), 4),
        )
        self.rule = rules.index('color')

    def getRule(self):
        return rules[self.rule]

    def setRule(self, rule):
        self.rule = rules.index(rule)

    def findMatch(self, card):
        #color
        if self.rule == rules.index('color'):
            for i, c in enumerate(self.cards):
                if c.color == card.color:
                    return i

        #shape
        if self.rule == rules.index('shape'):
            for i, c in enumerate(self.cards):
                if c.shape == card.shape:
                    return i

        #count
        if self.rule == rules.index('count'):
            for i, c in enumerate(self.cards):
                if c.count == card.count:
                    return i

game = GameSet()

def test():
    testcard = Card()
    print('Card: %s\n' % (testcard))
    for i, c in enumerate(game.cards):
        print('[Card %s] %s' % (i, c))
    print('---------------------')
    print ('Rule: %s' % (game.getRule()))
    print('Matching index is: %s' % (game.findMatch(testcard)))
    print('---------------------')
    game.setRule('shape')
    print ('Rule: %s' % (game.getRule()))
    print('Matching index is: %s' % (game.findMatch(testcard)))
    print('---------------------')
    game.setRule('count')
    print ('Rule: %s' % (game.getRule()))
    print('Matching index is: %s' % (game.findMatch(testcard)))

def playHand(card):
    winner = game.findMatch(card)
    cheat(card)
    print('Card: %s\n' % (card))
    for i, c in enumerate(game.cards):
        print('[Card %s] %s' % (i, c))
    print('---------------------')
    pick = int(raw_input('Which matches? :'))
    print('%s : %s' % (pick, winner))
    if pick == winner:
        print('-- Correct --')
        return 1
    else:
        print('-- Wrong --')
        return 0

def cheat(card):
    #call in playHand to reveal the rule and answer
    print('--- cheater ---')
    print ('Rule: %s' % (game.getRule()))
    print('Matching index is: %s' % (game.findMatch(card)))
    print('--- cheater ---')

if __name__ == '__main__':
    while 1:
        print('RESET')
        game.rule = np.random.randint(0,3)
        correct_answers = 0
        hands = 0
        while correct_answers < 4 and hands < 8:
            #pick a new rule and new card
            correct_answers += playHand(Card())
            hands += 1
